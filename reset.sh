#!/bin/bash

function stop()
{
	docker-compose stop
}

function remove()
{
	docker-compose stop && docker-compose rm -f
	rm -rf /home/www/easyswoole-admin/mysql/data/*
}

function start()
{
	docker-compose up -d
}

function restart()
{
	remove
	start
}

cd /home/www/easyswoole-admin

case "$1" in
"start")
	start
;;
"stop")
	stop
;;
"remove")
	remove
;;
"restart")
	restart
;;
*):
	echo "start|stop|remove|restart"
;;
esac

